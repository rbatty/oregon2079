'use strict';

var Game = (function() {
    //globals

    //var state = {};
    var dataLoaded = false;
    var tickLength = 1000;

    var 
        container,
        output,
        overlay,
        player,
        environment,
        tick,
        allLocations,
        selectionStore,
        ui,
        debug;
      
    //listen for onload finish and execute if data's ready
    function preCheck(){
      if (dataLoaded) {
        initDisplay();
      } else {
        window.addEventListener("data_loaded", titleState);
      }
    }

    function titleState(){
      clearDisplay();
      //draw title
      var title = document.createElement("h1");
      title.setAttribute("class","title");
      title.innerHTML = "OREGON 2079";

      var startButton = document.createElement("button");
      startButton.id = "start";
      startButton.innerHTML = "start";
      
      
      output.appendChild(title);
      output.appendChild(startButton);

      //wait for input
      //go to intro
      startButton.onclick = introState;
    }

    function introState(){
      clearDisplay();
      //create player
      player = createPlayer(true);
      player.companions = createPeople();
      player.site = createPlaces();

      //initialize environment
      environment = {
        "id": "env",
        "time": "10:00",
        "days": 0,
        "weather": "hot",
        "risk": "low"
      };

      //timeFormatter(environment.time)
      //intialize UI
      ui = Game.ui;
      ui.createModal("main");

      //draw intro
      var introData = Game.data.intro;
      var introduction = document.createElement("div");
      introduction.setAttribute("class", "scroll-text");
      var heading = document.createElement("h2");
      heading.innerHTML = introData.heading;
      introduction.appendChild(heading);
      for (var i=0; i<introData.content.length; i++) {
        var paragraph = document.createElement("p");
        paragraph.innerHTML = introData.content[i];
        introduction.appendChild(paragraph)
      }

      //TODO: abstract this out
      var nextButton = document.createElement("button");
      nextButton.id = "start";
      nextButton.innerHTML = "start";
      introduction.appendChild(nextButton);

      output.appendChild(introduction);
      createBackground();

      //TODO: player makes choices about starting options

      //TODO: goto map-loop or travel-state
      nextButton.onclick = travelState;
    }

    function mapOverlay() {
      //TODO: generate a map and player position
    }

    function travelState(){

      clearDisplay();
      drawTravelBackground();

      displayPlayerStatsHUD();
      displayEnvironmentStatsHUD();

      //start the main game loop
      tick = setInterval(travelLoop,tickLength);

      //overlay.addEventListener("main-modal-closed", restartTravelLoop);
      //output.addEventListener("click",travelPause);

      var menuButton = document.createElement("button");
      menuButton.setAttribute("class", "button-large");
      menuButton.innerHTML = "REST";
      menuButton.addEventListener("click",campState);
      output.appendChild(menuButton);
    }

    function travelStop(){
      clearInterval(tick);
      // var paused = document.createElement("p");
      // paused.innerHTML="Paused.";
      // ui.openModal("main", paused, null);
    }

    function restartTravelLoop(){
      tick = setInterval(travelLoop,tickLength);
    }

    function travelLoop(restart){
      // restart: Boolean
      incrementTime(30);
      //if(hours === "waking") {}
      updateConsumption(30);
      moveParty(30);

      //update party stats
      updatePlayerStatsHUD();
      //update time
      updateEnvironmentStatsHUD();

      if (!restart) {
        
      }
    }

    function updateConsumption(minutesElapsed){
      // timeElapsed:Int in minutes
      var hoursElapsed = minutesElapsed / 60;
      var foodConsumed = player.rationRate * player.companions.length * hoursElapsed;
      var waterConsumed = player.waterRate * player.companions.length * hoursElapsed;

      if (player.food.qty > 0) {
        player.food.qty = Math.round((player.food.qty - foodConsumed)*100)/100;
      } else {
        player.food.qty = 0;
      }

      if (player.water.qty > 0) {
        player.water.qty = Math.round((player.water.qty - waterConsumed)*100)/100;;
      } else {
        player.water.qty = 0;
      }

      if(player.food.qty <= 0) {
        console.warn("The party is starving!");
        //subtract health
        player.health = Math.round((player.health - 2 * foodConsumed)*100)/100;
      }
      if(player.water.qty <= 0) {
        console.warn("The party is dehydrated!");
        //subtract health
        player.health = Math.round((player.health - 10 * waterConsumed)*100)/100;
      }
      console.log("health: " + player.health);
      if (player.health <= 0) {
        clearInterval(tick);
        var dead = document.createElement("p");
        dead.innerHTML="Your party is dead.";
        ui.openModal("main", dead, null);
      }
    }

    function incrementTime(advance){
      // advance:Int - minutes to advance time by
      var current = environment.time.match(/\d{2}/g);  //make 2 item array with hours and minutes
      var hours = parseInt(current[0], 10);
      var minutes = parseInt(current[1], 10);
      var days = environment.days;
      minutes += advance;
      if (minutes >= 60) {
        minutes -= 60;
        hours++;
      }
      if (minutes == 0) {
        minutes = "00";
      }
      if (hours >= 24) {
        hours -= 24;
        days += 1;
      }
      if (hours < 10) {
        hours = "0" + hours;
      }
      var newTime = hours+":"+minutes;
      environment.time = newTime;
      environment.days = days;
    }

    function createBackground(){
      var background = document.createElement("div");   //this could be a canvas, but needed?
      background.id = "background";
      background.style.backgroundSize = "cover";
      background.innerHTML = "";
      container.appendChild(background);
    }

    function drawCampBackground(){
      var background = document.getElementById("background");  //probably want to store a reference instead
      background.innerHTML = "";
      background.style.backgroundImage = "url(assets/img/test_ot_camp.png)";
    }

    function drawTravelBackground(){
      var background = document.getElementById("background");  //probably want to store a reference instead
      background.innerHTML = "";
      background.style.backgroundImage = "url(assets/img/test_ot.jpg)";
      
      var player = document.createElement("div");
      player.id = "player-image";
      player.style.backgroundImage = "url(assets/img/walk_sprites.png)";
      player.className += "animate";

      background.appendChild(player)
      //return background;
    }

    function moveParty(minutesElapsed){
      //trigger walking animation and update miles tracker
      var hoursElapsed = minutesElapsed / 60;
      var milesTraveled = player.travelRate * hoursElapsed;
      player.miles = Math.round((player.miles + milesTraveled)*100)/100;

      //trigger a css animation
    }

    function displayPlayerStatsHUD(){
      var playerStatContainer = document.createElement("div");
      playerStatContainer.id = "player-stats";
      playerStatContainer.appendChild(createStatDisplay(player,"food", "Food"));
      playerStatContainer.appendChild(createStatDisplay(player,"water", "Water"));
      playerStatContainer.appendChild(createStatDisplay(player,"miles", "Miles Traveled"));

      output.appendChild(playerStatContainer);
/*      this.name = name || "player";
        this.companions = [];
        this.site = {};
        this.miles = 0;
        this.food = {"qty": 0, "unit": "lb"};
        this.water = {"qty": 0, "unit": "gal"};
        this.cash = {"qty": 0.0, "unit": "$"};
        this.materials = {"qty": 0, "unit": "parts"};
*/
    }

    function updatePlayerStatsHUD(){
      var display = document.getElementById("player-food-display");
      display.setAttribute("data-stat", player.food.qty + " " + player.food.unit);
      //display.innerHTML = "Food: " + player.food.qty + " " + player.food.unit;
      
      display = document.getElementById("player-water-display");
      display.setAttribute("data-stat", player.water.qty + " " + player.water.unit);
      //display.innerHTML = "Water: " + player.water.qty + " " + player.water.unit;

      display = document.getElementById("player-miles-display");
      display.setAttribute("data-stat", player.miles);
      //display.innerHTML = "Distance traveled: " + player.water.qty + " " + player.water.unit;
    }

    function displayEnvironmentStatsHUD() {
      var envStatContainer = document.createElement("div");
      envStatContainer.id = "env-stats";
      var timeDisplay =  document.createElement("div");
      timeDisplay.id = "env-time-display"
      timeDisplay.innerHTML = "Time: " + environment.time;
      envStatContainer.appendChild(timeDisplay);
      output.appendChild(envStatContainer);
    }

    function createStatDisplay(object, stat, name){
      var display =  document.createElement("div");
      display.id = object.id + "-" + stat + "-display";
      display.setAttribute("class", "stat-display");
      display.innerHTML = name + ": ";
      return display;
    }

    function updateEnvironmentStatsHUD(){
      var timeDisplay = document.getElementById("env-time-display");
      timeDisplay.innerHTML = "Time: " + environment.time;
    }

    function campState(){
      travelStop();
      clearDisplay();
      selectionStore.reset();   //clear out old store data
      drawCampBackground();

      var companionContainer = ui.createCompanionsDisplay(player.companions);
      output.appendChild(companionContainer);

      var locationContainer = ui.createLocationsDisplay(player.site);
      output.appendChild(locationContainer);

      //submit all the selections
      var submitButton = document.createElement("button");
      submitButton.id = "submit-button";
      submitButton.innerHTML = "submit";
      submitButton.onclick = validateSubmission;
      output.appendChild(submitButton);

      //create way back to travel
      var menuButton = document.createElement("button");
      menuButton.setAttribute("class", "button-large");
      menuButton.innerHTML = "TRAVEL";
      menuButton.addEventListener("click",travelState);
      output.appendChild(menuButton);
    }

  function initDisplay(){

    //debug helper
    debug = document.getElementById("debug");

    //draw to 
    container = document.getElementById("screen");

    output = document.createElement("div");
    output.id = "foreground";

    container.appendChild(output);

    overlay = document.createElement("div");
    overlay.setAttribute("class","overlay hidden");
    overlay.id = "overlay";

    container.appendChild(overlay);

    titleState();
  }

  function clearDisplay(){
    output.innerHTML = "";
  }

  function validateSubmission(e){
    var isValid = selectionStore.validate({
      fields: ["place","person","action"], 
      dependency: ["action","person"]
    })
    debug.innerHTML = isValid;
    //TODO: give some kind of error feedback
    if (isValid.success) {
      var results = getActionResults();
      player.updateGoods(results);
      formatResultsForDisplay(results);
      campState();//reset the ui
      console.log(player);
    } else {
      console.warn(isValid.error);
    }
  }

  function getActionResults(){
    var results = [];
    for (var i = selectionStore.data.length-1; i>=0; i--) {
        var person = selectionStore.data[i]["person"];
        var action = selectionStore.data[i]["action"];
        if (person) {
        //for selected action, get person object skill level
            for (var j=player.companions.length-1; j>=0; j--){
                if(player.companions[j].name === person) {
                    var skillLevel = player.companions[j].getSkill(action);
                    var success = determineSuccess(skillLevel);
                    var goods = getGoodsGained(success, action, skillLevel);
                    results.push({"person":person, "action":action, "success":success, "goods": goods});
                }
            } 
        }
    }
    return results;
  }

  function formatResultsForDisplay(results){
    console.log(results);
    var ul = document.createElement("ul");
    for (var i = results.length-1; i >= 0; i--){
        var li = document.createElement("li");
        var successPhrase = (results[i].success ? " was successful." : " failed.");
        li.innerHTML = results[i].person +"'s attempt to " + results[i].action + successPhrase;
        if (results[i].success) {
          li.innerHTML += " The party gained "+ results[i].goods.qty + " " + results[i].goods.type + ".";
        }
        ul.appendChild(li);
    }
    ui.openModal("main", ul);
    //store all transactions in a log somewhere?
  }

  function getGoodsGained(success, action, skill) {
    var type;
    var quantity = 0;
    // elegant enough?
    switch (action) {
      case "dowse":
        type = "water";
        break
      case "forage":
        type = "food";
        break
      case "scavenge":
        type = "materials";
        break
    }
    if (success) {
      var max = 8;
      var min = 1;
      quantity = Math.floor(Math.random()*(max-min)) + min;
      //skill give chance at bonus
      var chanceAtBonus = Math.floor(Math.random()*(99)) + 1;
      if (chanceAtBonus < skill) {
        var bonus = Math.floor(Math.random()*(3-1)) + 1;
        quantity += bonus;
      }
    }
    return {"type": type, "qty": quantity}
  }

  function determineSuccess(skillLevel, difficulty){
      //eventually, locations might have difficulties to them
    var random = Math.floor(Math.random()*100)+1;
    if (parseInt(skillLevel) >= random){
      return true;
    } else {
      return false;
    }
  }

  function reset(){
      selectionStore.reset();
  }

    function createPlayer(isNew){
        if (!isNew) {
            //TODO: load save data
            return false
        }
        return new Player();
    }

    function createPeople(){
        var jess = new Person("Jess", 20, "tinkerer");
        var jorge = new Person("Jorge", 44, "farmer");
        var jasmine = new Person("Jasmine", 14, "dowse");
        var judith = new Person("Judith", 54, "dowse");
        var jerry = new Person("Jerry", 9, "dowse");

        // all people
        var allCompanions = [jess, jorge, jasmine, judith, jerry];
        jasmine.updateSkill("dowse","90");

        // player 'inventory' of people
        return [jess, jorge, jasmine];
    }

    function createPlaces(){

        var creek = new Place("creek");
        var forest = new Place("forest");
        var abandoned = new Place("abandoned");

        // location have associated actions
        creek.specialActivity("dowse");

        // all locations
        allLocations = [creek, forest, abandoned];
        // 'inventory' of locations
        var currentSite = new Site("Rockford", [creek, forest, abandoned]);
        return currentSite;
    }

    function createStore(){
      return new Store("place",["place","person","action"]);
    }

    /* CLASS DEFINITIONS */

    var Player = function(name){
        this.id = "player",
        this.name = name || "Alex";
        this.companions = [];
        this.site = {};
        this.miles = 0;
        this.rationRate = 0.166;  // lbs/person/waking hour
        this.waterRate = 0.045;   // gal/person/waking hour
        this.travelRate = 3.0;   // miles per hour
        this.health = 50;
        this.food = {"qty": 10, "unit": "lb"};
        this.water = {"qty": 5, "unit": "gal"};
        this.cash = {"qty": 0.0, "unit": "$"};
        this.materials = {"qty": 0, "unit": "parts"};
    }

    Player.prototype = {
      updateGoods: function(data){
        // data should be array of objects containing a goods object with {type, qty}
        for (var i=data.length-1; i>=0; i--) {
          var type = data[i].goods.type;
          var quantity = data[i].goods.qty;
          this[type].qty += quantity;
        }
      }
    }

    //people data
    var Person = function(name, age, occupation, skill){
        this.name = name;
        this.age = age;
        this.skills = [
            { skill: "forage", level: 10 },
            { skill: "scavenge", level: 10 }
        ];
        this.occupation = occupation;
        this.skill = 0; //0-100
        if (occupation) {
            this.skills.push({ skill: occupation, level: skill || 0 })
        }
    }

    Person.prototype = {
        rename: function(name){
        this.name = name
        },
        getOccupation: function(){
            return this.occupation;
        },
        updateSkill: function(skill,modifier){
            for (var i = this.skills.length-1; i >= 0; i--){
                if (this.skills[i].skill === skill){
                  var splitModifier = modifier.split(/(\D)(\d*)/).filter(Boolean);
                  if(splitModifier.length > 1) {
                    switch (splitModifier[0]) {
                    case "+":
                        this.skills[i].level += parseInt(splitModifier[1],10);
                    break
                    case "-":
                        this.skills[i].level -= parseInt(splitModifier[1],10);
                      this.skills[i].level = this.skills[i].level < 0 ? 0 : this.skills[i].level;
                    break
                  }
                } else {
                    this.skills[i].level = modifier;
                }
              }
              return true;
            }
            console.error("Skill update failed: skill does not exist. Did you mean to addSkill?");
            return false;
        },
        addSkill: function(skill, initialValue){
            this.skills.push({skill: skill, level: initialValue || 0});
        },
        getSkill: function(skill){
            for(var i=this.skills.length - 1; i>=0; i--){
                if (this.skills[i].skill === skill){
                return this.skills[i].level;
              }
            }
        }
    }

    //location data
    var Site = function(name, locations){
        this.name = name;
        this.locations = locations;
    }

    var Place = function(name){
        this.name = name;
        this.activities = ["scavenge","forage"];
        this.locked = [];
        this.occupied = null;
    }

    Place.prototype = {
        specialActivity: function(activity){
        this.locked.push(activity);
      }
    }

    //Store for current player selections
    var Store = function(key, model){
        this.data = [];
        this.key = key;
        this.model = model;
    };

    Store.prototype = {
      update: function(newData){
        //data validation
        var props = Object.keys(newData).sort();
        var valid = 0;
        for (var i = props.length-1;i>=0;i--){
            if (this.model.sort().indexOf(props[i]) > -1) {
            valid++;
          }
        }
        if (valid === props.length){
          var rowExists = false;
          for   (var i = this.data.length-1; i>=0; i--) {
            if(!newData[this.key]) {
                console.error("Store update failed: bad key value.");
              return false
            }
            if (this.data[i][this.key] === newData[this.key]) {
              for (var key in newData) {
                this.data[i][key] = newData[key];
              }
              rowExists = true;
              break;
            }
          }
          if(!rowExists) {
            this.data.push(newData)
          }
        } else {
            console.error("Store update failed: data does not fit model.");
        }
      },
      lookupBy: function(key, value){
        for (var i = this.data.length-1; i>=0; i--) {
            if (this.data[i][key] === value) {
            return this.data[i];
          }
        }
            return false;
      },
      reset: function(){
        this.data = [];
      },
      validate: function(required){
        var fields = required.fields;
        var dependency = required.dependency;
        if (this.data.length < 1) {
          return {"success": false, "error": "No data submitted."};
        }
        for (var i = this.data.length-1; i>=0; i--){
            for (var j = fields.length-1; j>=0; j--) {
            //place required, if person then action
            if (fields[j] === dependency[0]) {
                if(!this.data[i][dependency[0]] && this.data[i][dependency[1]]) {
                return {"success": false, "error": "Missing field: " + fields[j] + "."};
              }
            }
          }
        }
        return {"success": true};
      }
    }

    /* UTILITIES & FORMATTERS */

    /* START EXECUTION */
    // because function expressions are never hoisted!

    window.onload = preCheck;

    //create game data
    selectionStore = createStore();

    //notify that the data is loaded
    dataLoaded = true;
    var loadEvent = new Event("data_loaded");
    window.dispatchEvent(loadEvent);

    return {
      selectionStore : selectionStore
    };
})()
