Game.ui = Game.ui || (function(){
	console.log("ui initialized");

	function createCompanionsDisplay(companions){
		/* var companions = Array : player.companions */

		var companionContainer = document.createElement("div");
		companionContainer.id = "container-companions";
		for (var i = companions.length-1; i>=0; i--){
		  var element = document.createElement("div");
		  element.className = "icon-companion";
		  element.innerHTML = companions[i].name;
		  element.data = {"companion": companions[i]};
		  element.id = companions[i].name;
		  element.draggable = true;
		  
		  element.ondragstart = function(e){
		  		e.target.data.parent = e.srcElement.parentElement.id;
		        e.dataTransfer.setData("companion-data",JSON.stringify(e.target.data));
		  };
		  companionContainer.appendChild(element);
		}

		//make companion container droppable
		companionContainer.ondrop = function(e){
		    e.preventDefault();
		    var data = JSON.parse(e.dataTransfer.getData("companion-data"));
		    var person = data.companion;
		    var el = e.target;
		    var currentAssignment = Game.selectionStore.lookupBy("person",person.name);

		    //clear out occupied class && options
		    document.getElementById(currentAssignment.place).innerHTML = currentAssignment.place;
		    document.getElementById(currentAssignment.place).className = "icon-location";
		    el.appendChild(document.getElementById(person.name));
		    Game.selectionStore.update({"place":currentAssignment.place,"person":null,"action":null});
		}
		companionContainer.ondragover = function(e){
		    e.preventDefault();
		}

		return companionContainer;
	}

	function createLocationsDisplay(currentSite){
		/* var currentSite = Site : player.site */

		var locationContainer = document.createElement("div");
		locationContainer.className = "container-locations";
		for (var i = currentSite.locations.length-1; i >= 0; i--){
		    var row = document.createElement("div");
		    row.className = "row location"
		    var element = document.createElement("div");
		    element.className = "icon-location";
		    element.innerHTML = currentSite.locations[i].name;
		    element.id = currentSite.locations[i].name;
		    element.data = currentSite.locations[i];

		    element.ondrop = function(e){
		        e.preventDefault();
		        var data = JSON.parse(e.dataTransfer.getData("companion-data"));
		    	var person = data.companion;
		    	var parent = data.parent;
		        var place = e.target.data;
		        var el = e.target;

		  		if (parent !== "container-companions") {
		  			var currentAssignment = Game.selectionStore.lookupBy("person",person.name);
				    //clear out occupied class && options -- this is repeated: abstract this out?
				    document.getElementById(currentAssignment.place).innerHTML = currentAssignment.place;
				    document.getElementById(currentAssignment.place).className = "icon-location";
				    el.appendChild(document.getElementById(person.name));
				    Game.selectionStore.update({"place":currentAssignment.place,"person":null,"action":null});
		  		}
		        assignTarget(person, place, el);
		    }
		    element.ondragover = function(e){
		        e.preventDefault();
		        //console.log("dragged over")
		    }
		    row.appendChild(element);
		    locationContainer.appendChild(row);
		}

		return locationContainer;
	}

	// ability to assign people to locations w/ activity
	function assignTarget(person, place, element){
		console.log(Game.selectionStore);
	  //check if already occupied
	  var placeData = Game.selectionStore.lookupBy("place", place.name);
	    if (placeData && placeData.person) {
	    console.log("occupied");
	  } else {
	  	//can I get the previous parent to make sure it's cleared out
	    console.log("dropped " + person.name + " at "+ place.name);
	    Game.selectionStore.update({"place":place.name,"person":person.name,"action":null});
	    element.parentNode.appendChild(document.getElementById(person.name));
	    drawOptionsMenu(person, place, element);
	  }
	}

	function drawOptionsMenu(person, place, element){
	    var options = place.activities;
	  var special = place.locked;
	  if (special && person.occupation === special[0]) {
	    options.push(special);
	  }
	  //option button selection updates the store
	    for (var i = options.length-1; i>=0; i--){
	    var option = document.createElement("div");
	    option.className = "button-option";
	    option.innerHTML = options[i];
	    option.id = options[i];
	    option.onclick = function(e){
	      var option = e.currentTarget.id;
	      var place = e.currentTarget.parentNode.id;
	      Game.selectionStore.update({"place": place, "action": option});
	      e.currentTarget.classList.toggle("selected");
	    }
	    element.appendChild(option);
	  }
	}

	// MODALS

	function createModal(name){
		// name: String
		
	  var modal = document.createElement("div");
	  modal.setAttribute("class","modal");
	  modal.id = name + "-modal";
	  var closeButton = document.createElement("button");
	  closeButton.setAttribute("class","modal-close");
	  closeButton.innerHTML = "X";
	  closeButton.addEventListener("click", closeModal);
	  // TODO: add event listener
	  var contentArea = document.createElement("div");
	  contentArea.setAttribute("class","modal-content");
	  modal.appendChild(closeButton);
	  modal.appendChild(contentArea);
	  overlay.appendChild(modal);
	}

	function openModal(name, content, type){
		// name:String - must match the name of a modal created with createModal()
		// content:HtmlNode 
	  // type is optional, not currently used

	  // maps results to correct modal and shows the modal

	  var modalContainer = document.getElementById(name+"-modal");
	  var modalContent = modalContainer.getElementsByClassName("modal-content")[0];
	  modalContent.innerHTML = "";  //initialize container
	  modalContent.appendChild(content);
	  document.getElementById("overlay").classList.toggle("hidden");
	}

	function closeModal(e){
	    // wipe data here or just overwrite?
	    var el = e.target.parentElement.id;
	    var overlay = document.getElementById("overlay");
	    var dismissEvent = new Event(el + "-closed");
    	overlay.dispatchEvent(dismissEvent);
	    overlay.classList.toggle("hidden");

	}

	return {
		assignTarget : assignTarget,
		createCompanionsDisplay : createCompanionsDisplay,
		createLocationsDisplay : createLocationsDisplay,
		createModal : createModal,
		openModal : openModal
	}
})()