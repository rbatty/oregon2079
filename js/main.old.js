
//debug helper
var debug = document.getElementById("debug");

//draw to 
var output = document.getElementById("output");

var overlay = document.createElement("div");
overlay.setAttribute("class","overlay hidden");
overlay.id = "overlay";
output.appendChild(overlay);


var Player = function(name){
    this.name = name || "player";
  this.companions = [];
  this.site = {};
}

var player = new Player();

//people data
var Person = function(name, age, occupation, skill){
    this.name = name;
  this.age = age;
  this.skills = [
    { skill: "forage", level: 10 },
    { skill: "scavenge", level: 10 }
  ];
  this.occupation = occupation;
  this.skill = 0; //0-100
  if (occupation) {
    this.skills.push({ skill: occupation, level: skill || 0 })
  }
}

Person.prototype = {
    rename: function(name){
    this.name = name
  },
  getOccupation: function(){
    return this.occupation;
  },
  updateSkill: function(skill,modifier){
    for (var i = this.skills.length-1; i >= 0; i--){
        if (this.skills[i].skill === skill){
        var splitModifier = modifier.split(/(\D)(\d*)/).filter(Boolean);
                if(splitModifier.length > 1) {
            switch (splitModifier[0]) {
            case "+":
                this.skills[i].level += parseInt(splitModifier[1],10);
            break
            case "-":
                this.skills[i].level -= parseInt(splitModifier[1],10);
              this.skills[i].level = this.skills[i].level < 0 ? 0 : this.skills[i].level;
            break
          }
        } else {
            this.skills[i].level = modifier;
        }
        console.log(splitModifier);
      }
      return true;
    }
    console.error("Skill update failed: skill does not exist. Did you mean to addSkill?");
    return false;
  },
  addSkill: function(skill, initialValue){
    this.skills.push({skill: skill, level: initialValue || 0});
  },
  getSkill: function(skill){
    for(var i=this.skills.length - 1; i>=0; i--){
        if (this.skills[i].skill === skill){
        return this.skills[i].level;
      }
    }
  }
}

var jess = new Person("Jess", 20, "tinkerer");
var jorge = new Person("Jorge", 44, "farmer");
var jasmine = new Person("Jasmine", 14, "dowse");
var judith = new Person("Judith", 54, "dowse");
var jerry = new Person("Jerry", 9, "dowse");

// all people
var allCompanions = [jess, jorge, jasmine, judith, jerry];
jasmine.updateSkill("dowse","50");

// player 'inventory' of people
player.companions = [jess, jorge, jasmine];

//location data
var Site = function(name, locations){
    this.name = name;
    this.locations = locations;
}

var Place = function(name){
    this.name = name;
  this.activities = ["scavenge","forage"];
  this.locked = [];
  this.occupied = null;
}

Place.prototype = {
    specialActivity: function(activity){
    this.locked.push(activity);
  }
}

var creek = new Place("creek");
var forest = new Place("forest");
var abandoned = new Place("abandoned");

// all locations
var allLocations = [creek, forest, abandoned];
// 'inventory' of locations
var currentSite = new Site("Rockford", [creek, forest, abandoned]);
player.site = currentSite;

// location have associated actions
creek.specialActivity("dowse");

// certain people can activate special actions in certain locations

//Store for current player selections
var Store = function(key, model){
  this.data = [];
  this.key = key;
  this.model = model;
};

Store.prototype = {
  update: function(newData){
    //data validation
    var props = Object.keys(newData).sort();
    var valid = 0;
    for (var i = props.length-1;i>=0;i--){
        if (this.model.sort().indexOf(props[i]) > -1) {
        valid++;
      }
    }
    if (valid === props.length){
      var rowExists = false;
      for   (var i = this.data.length-1; i>=0; i--) {
        if(!newData[this.key]) {
            console.error("Store update failed: bad key value.");
          return false
        }
        if (this.data[i][this.key] === newData[this.key]) {
            //console.log("key " + this.key + " exists");
          for (var key in newData) {
            this.data[i][key] = newData[key];
          }
          rowExists = true;
          break;
        }
      }
      if(!rowExists) {
        this.data.push(newData)
      }
    } else {
        console.error("Store update failed: data does not fit model.");
    }
  },
  lookupBy: function(key, value){
    for (var i = this.data.length-1; i>=0; i--) {
        if (this.data[i][key] === value) {
        return this.data[i];
      }
    }
        return false;
  },
  reset: function(){
    this.data = [];
  },
  validate: function(required){
    var fields = required.fields;
    var dependency = required.dependency;
    for (var i = this.data.length-1; i>=0; i--){
        for (var j = fields.length-1; j>=0; j--) {
        //place required, if person then action
        if (fields[j] === dependency[0]) {
            if(!this.data[i][dependency[0]] && this.data[i][dependency[1]]) {
            return false;
          }
        }
      }
    }
    return true;
  }
}

var selectionStore = new Store("place",["place","person","action"]);

// ui
// display companions
var companionContainer = document.createElement("div");
companionContainer.className = "container-companions";
for (var i = player.companions.length-1; i>=0; i--){
  var element = document.createElement("div");
  element.className = "icon-companion";
  element.innerHTML = player.companions[i].name;
  element.data = player.companions[i];
  element.id = player.companions[i].name;
  element.draggable = true;
  element.ondragstart = function(e){
        e.dataTransfer.setData("thing",JSON.stringify(e.target.data));
  };
  companionContainer.appendChild(element);
}

//make companion container droppable
companionContainer.ondrop = function(e){
    e.preventDefault();
    var person = JSON.parse(e.dataTransfer.getData("thing"));
    var el = e.target;
    var currentAssignment = selectionStore.lookupBy("person",person.name);
    //clear out occupied class && options
    document.getElementById(currentAssignment.place).innerHTML = currentAssignment.place;
    document.getElementById(currentAssignment.place).className = "icon-location";
    el.appendChild(document.getElementById(person.name));
    selectionStore.update({"place":currentAssignment.place,"person":null,"action":null});
}
companionContainer.ondragover = function(e){
    e.preventDefault();
}
output.appendChild(companionContainer);

// create location list
var locationContainer = document.createElement("div");
locationContainer.className = "container-locations";
for (var i = currentSite.locations.length-1; i >= 0; i--){
    var row = document.createElement("div");
    row.className = "row location"
    var element = document.createElement("div");
    element.className = "icon-location";
    element.innerHTML = currentSite.locations[i].name;
    element.id = currentSite.locations[i].name;
    element.data = currentSite.locations[i];
    element.parent = this.parent;
    element.ondrop = function(e){
        e.preventDefault();
        var person = JSON.parse(e.dataTransfer.getData("thing"));
        var place = e.target.data;
        var el = e.target;
        assignTarget(person, place, el);
    }
    element.ondragover = function(e){
        e.preventDefault();
        //console.log("dragged over")
    }
    row.appendChild(element);
    locationContainer.appendChild(row);
}
output.appendChild(locationContainer);

// ability to assign people to locations w/ activity
function assignTarget(person, place, element){
  //check if already occupied
  var placeData = selectionStore.lookupBy("place", place.name);
    if (placeData && placeData.person) {
    console.log("occupied");
  } else {
    console.log("dropped " + person.name + " at "+ place.name);
    selectionStore.update({"place":place.name,"person":person.name,"action":null});
    element.parentNode.appendChild(document.getElementById(person.name));
    drawOptionsMenu(person, place, element);
  }
}

function drawOptionsMenu(person, place, element){
    var options = place.activities;
  var special = place.locked;
  if (special && person.occupation === special[0]) {
    options.push(special);
  }
  //option button selection updates the store
    for (var i = options.length-1; i>=0; i--){
    var option = document.createElement("div");
    option.className = "button-option";
    option.innerHTML = options[i];
    option.id = options[i];
    option.onclick = function(e){
        var option = e.currentTarget.id;
        var place = e.currentTarget.parentNode.id;
      selectionStore.update({"place": place, "action": option});
    }
    element.appendChild(option);
  }
}

function createModal(name){
  var modal = document.createElement("div");
  modal.setAttribute("class","modal");
  modal.id = name + "-modal";
  var closeButton = document.createElement("button");
  closeButton.setAttribute("class","modal-close");
  closeButton.innerHTML = "X";
  closeButton.addEventListener("click", closeModal);
  // TODO: add event listener
  var contentArea = document.createElement("div");
  contentArea.setAttribute("class","modal-content");
  modal.appendChild(closeButton);
  modal.appendChild(contentArea);
  overlay.appendChild(modal); 
}

function openModal(name, content, type){
  //type may not be necessary
  //map results to correct modal and show the modal

  var modalContainer = document.getElementById(name+"-modal");
  var modalContent = modalContainer.getElementsByClassName("modal-content")[0];
  modalContent.innerHTML = "";  //initialize container
  modalContent.appendChild(content);
  document.getElementById("overlay").classList.toggle("hidden");
}

function closeModal(e){
    // wipe data here or just overwrite?
    document.getElementById("overlay").classList.toggle("hidden");
}

function formatResults(results){
    console.log(results);
    var ul = document.createElement("ul");
    for (var i = results.length-1; i >= 0; i--){
        var li = document.createElement("li");
        var successPhrase = (results[i].success ? " was successful." : " failed.");
        li.innerHTML = results[i].person +"'s attempt to " + results[i].action + successPhrase;
        ul.appendChild(li);
    }
    openModal("outcome", ul);
    //store all transactions in a log somewhere?
}

createModal("outcome");

//submit all the selections
document.getElementById("submit").onclick = function(e){
  //check if action has been selected for all rows with people
  var isValid = selectionStore.validate({
    fields: ["place","person","action"], 
    dependency: ["action","person"]
  })
  debug.innerHTML = isValid;
  if (isValid) {
    var results = processSelections();
    formatResults(results);
  }
}

function processSelections(){
    var results = [];
    for (var i = selectionStore.data.length-1; i>=0; i--) {
        var person = selectionStore.data[i]["person"];
        var action = selectionStore.data[i]["action"];
        if (person) {
        //for selected action, get person object skill level
            for (var j=player.companions.length-1; j>=0; j--){
                if(player.companions[j].name === person) {
                    var skillLevel = player.companions[j].getSkill(action);
                    var success = determineSuccess(skillLevel);
                    results.push({"person":person, "action":action, "success":success});
                }
            } 
        }
    }
    return results;
}

function determineSuccess(skillLevel, difficulty){
    //eventually, locations might have difficulties to them
  var random = Math.floor(Math.random()*100)+1;
  if (parseInt(skillLevel) >= random){
    return true;
  } else {
    return false;
  }
}

function reset(){
    selectionStore.reset();
}

//output displayed