Game.data = {};
Game.data.intro = {
	"heading" : "Oregon 2079",
	"content" : [
		"We all heard the warnings that climate change was coming, but we had no idea what it would mean.",
		"I've lived in Chicago all my life. I can still remember a time when I was six, sitting on one of the lakefront beaches with my family, eating a popsicle and basking in the sun.",
		"Those happy days on the beach are long gone now. The city now is a city divided. North siders seem to do all right--they still have jobs make money, can buy food and water. But millions of us aren't so lucky.",
		"For as long as I've been alive, or so my momma tells me, temperatures have being going up and up. It's a gradual change, so you don't notice at first. But in the last few years, the effects of that change have become more and more noticeable.",
		"The most obvious change is how hot it's getting. Or rather, how miserable it feels when you're too poor to afford air conditioning. Electricity is really expensive these days, and even if you can afford to cool your home, you can expect the power grid to fail at least once a week. The really well-to-do folks have their own generators to keep from breaking a sweat through even the hottest days of summer, but no one I know can afford that luxury.",
		"The power outages aren't just bad for keeping cool, their bad for keeping safe, too. There's lots of poor, desperate people these days, and when the lights go out at night, some of those people decide to use the cover of darkness to steal things. And some of them don't care who tries to get in their way. So when the power goes out at night, we rush to lock all our doors and windows, no matter how hot it is outside.",
		"You'd think that when times got tough, people would band together instead of robbing each other, but the truth is, there's just too many poor folk for banding together to do much good. The temperatures haven't just risen in Chicago, but everywhere, and some places that used to be home to a lot of people are underwater now. In the last year alone, it seems like our neighborhood has become home to more refugees from Louisiana than people left in Louisiana.",
		"Of course, those people all came to Chicago hoping for safety and opportunity, but unless they were rich to begin with, all they found here was expensive food, expensive water, a half-dried up lake, and a sewer system overflowing with human waste. Some land of opportunity.",
		"That's why I've decided that it's time to leave this city I call home and seek my own opportunities by heading to Oregon. I know the Pacific Northwest has it's own share of problems, with Seattle being half under water now, and the most rivers overfished, but at least along the coast the temperatures aren't over 100 degrees for most of the summer, the air is clean, and the land isn't overrun with the destitute and the dangerous.",
		"I hope I'm not fooling myself like the refugees who come to Chicago. But hope in this city is running out..."
		]
	};
